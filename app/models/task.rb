class Task < ApplicationRecord
  validates :name, presence: true
  validates :status, :numericality => { :greater_than_or_equal_to => 0 }
end