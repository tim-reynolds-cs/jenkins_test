require 'test_helper'

class TaskTest < ActiveSupport::TestCase

  test "should not save task without name" do    
    task = Task.new
    assert_not task.save, "Saved the task without a name"
  end 
  
  test "should not save task with negative status" do    
    task = Task.new
    task.status = -1
    assert_not task.save, "Saved the task with a negative status"
  end 
  
end
